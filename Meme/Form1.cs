﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Meme
{
    public partial class Генератор : Form
    {
        public Генератор()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.BackColor = Color.Black;
            label1.ForeColor = Color.Green;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!comboBox1.Items.Contains(comboBox1.Text))
                {   
                if(comboBox1.Text == "")
                {
                    MessageBox.Show("Введите слово");
                    return;
                }
                comboBox1.Items.Add(comboBox1.Text);
                }
            else
            {
                MessageBox.Show("Такое слово уже есть");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
                comboBox1.Items.Remove(comboBox1.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!comboBox2.Items.Contains(comboBox2.Text))
            {
                if (comboBox2.Text == "")
                {
                    MessageBox.Show("Введите слово");
                    return;
                }
                comboBox2.Items.Add(comboBox2.Text);
            }
            else
            {
                MessageBox.Show("Такое слово уже есть");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            comboBox2.Items.Remove(comboBox2.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!comboBox3.Items.Contains(comboBox3.Text))
            {
                if (comboBox3.Text == "")
                {
                    MessageBox.Show("Введите слово");
                    return;
                }
                comboBox3.Items.Add(comboBox3.Text);
            }
            else
            {
                MessageBox.Show("Такое слово уже есть");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            comboBox3.Items.Remove(comboBox3.Text);
            
            
        }

        private void buttonGeneration_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();

            textBox.Text =$"{comboBox1.Items[rnd.Next(0,comboBox1.Items.Count)]} {comboBox2.Items[rnd.Next(0, comboBox2.Items.Count)]} {comboBox3.Items[rnd.Next(0, comboBox3.Items.Count)]}";
        }
    }
}
